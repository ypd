//
// C Interface: config
//
// Description: 
//
//
// Author: Felix Bechstein <f@ub0r.de>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#define VERSION "$HEAD$"

#define MAX_QUEUE 20

#define TYPE_UNDEFINED 0
#define TYPE_HTTPS 1
#define TYPE_SSH 2
#define TYPE_HSTOP3G 3

#define DEFAULT_PORT 4443
#define DEFAULT_TRAFFIC TYPE_HTTPS
#define DEFAULT_NOTRAFFIC TYPE_SSH

#define DEFAULT_PORT_SSH 22
#define DEFAULT_PORT_HTTPS 443
#define DEFAULT_PORT_HSTOP3G 8980
#define DEFAULT_HOST "127.0.0.1"

#define BUFFSIZE 256
#define CONNECTION_TIMEOUT 3;
