//
// C Interface: session
//
// Description: 
//
//
// Author: Felix Bechstein <f@ub0r.de>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

//#define HSTOP3G_PATTERN {0x13 0x01 0x00 0x00 0x27}
//#define HSTOP3G_PATTERN_LEN 5

// this start a session with 2 threads/forks to read and write from the socket
void session_start(int sockd);

extern char* host;
extern int port;

extern int port_https;
extern int port_ssh;
extern int port_hstop3g;
