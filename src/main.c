
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/wait.h>

#include "config.h"
#include "session.h"

void grepenv()
{
	// grep listen port from env
	if (getenv("YPD_PORT"))
		port = atoi(getenv("YPD_PORT"));
	else
		port = DEFAULT_PORT;
	
	// grep target host from env
	if (getenv("YPD_HOST"))
		host = getenv("YPD_HOST");
	else
		host = DEFAULT_HOST;
	
	// grep https port from env
	if (getenv("YPD_PORT_HTTPS"))
		port_https = atoi(getenv("YPD_PORT_HTTPS"));
	else
		port_https = DEFAULT_PORT_HTTPS;
	
	// grep ssh port from env
	if (getenv("YPD_PORT_SSH"))
		port_ssh = atoi(getenv("YPD_PORT_SSH"));
	else
		port_ssh = DEFAULT_PORT_SSH;

	// grep hstop-3g port from env
	if (getenv("YPD_PORT_HSTOP3G"))
		port_hstop3g = atoi(getenv("YPD_PORT_HSTOP3G"));
	else
		port_hstop3g = DEFAULT_PORT_HSTOP3G;
}

int main(int argc, char *argv[])
{
	grepenv();
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in server;
	if (sock < 0)
	{
		perror("error opening socket");
		exit(EXIT_FAILURE);
	}
	
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(port);
	if (bind(sock, (struct sockaddr *) &server, sizeof(struct sockaddr_in)))
	{
		perror("error binding socket");
		exit(EXIT_FAILURE);
	}
	listen(sock, MAX_QUEUE);
	printf("listen on port %d\n", port);
	int sockd;
	while (( sockd = accept(sock,0,0)) > 0)
	{
		printf("incomming connection: %d\n", sockd);
		if (fork() == 0)
		{
			close(0);
			close(sock);
			session_start(sockd);
			exit(EXIT_SUCCESS);
		}
		close(sockd);
		while (waitpid(-1, NULL, WNOHANG))
			{}
	}
	close(sock);
	return EXIT_SUCCESS;
}
