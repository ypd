//
// C Implementation: session
//
// Description: 
//
//
// Author: Felix Bechstein <f@ub0r.de>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/wait.h>

#include "config.h"
#include "session.h"

char* host;
int port;

int port_https;
int port_ssh;
int port_hstop3g;

void session_get_type(int sockd, int *port, char* buffer, int buflen, int *l)
{
	*port = 0; // initial value
	
	int traffic_type = TYPE_UNDEFINED;
	*l = recv(sockd, buffer, buflen, MSG_DONTWAIT); // MSG_PEEK does not work here! -> return buffer and send it to client later
	
	if (*l < 0) // error occured (no data in queue)
	{
		// try again after a little while
		unsigned int s = CONNECTION_TIMEOUT;
		sleep(s);
		*l = recv(sockd, buffer, buflen, MSG_DONTWAIT);
	}
	
	if (*l == 0) // socket is closed
		return;
	
	if (*l > 0) // data in there. so let's analyse it
	{
		traffic_type = DEFAULT_TRAFFIC;
		
		// test for hstop3g traffic -- start
		char HSTOP3G_PATTERN[] = {0x13,0x01,0x00,0x00,0x27};
		int HSTOP3G_PATTERN_LEN = 5;

		int minl = *l;
		if (*l > HSTOP3G_PATTERN_LEN)
			minl = HSTOP3G_PATTERN_LEN;
		traffic_type = TYPE_HSTOP3G;
		int i;
		for (i = 0; i < minl; i++) {
			if (buffer[i] != HSTOP3G_PATTERN[i])
				traffic_type = DEFAULT_TRAFFIC;
		}
		// test for hstop3g traffic -- end		
	} else {
		traffic_type = DEFAULT_NOTRAFFIC;
	}
	
	switch (traffic_type)
	{
		case TYPE_HTTPS:
			printf("%d:\ttype https\n", sockd);
			*port = port_https;
			break;
		case TYPE_SSH:
			printf("%d:\ttype ssh\n", sockd);
			*port = port_ssh;
			break;
		case TYPE_HSTOP3G:
			printf("%d:\ttype hsopt-3g\n", sockd);
			*port = port_hstop3g;
			break;
		default:
			printf("%d:\ttype error!\n", sockd);
			*port = 0;
			break;
	}
	printf("%d:\tredirect to %s:%d\n", sockd, host, *port);
}

void session_forward(int sock_read, int sock_write)
{
	size_t l;
	char* buffer=(char*) malloc (BUFFSIZE);
	while ((l = recv(sock_read, buffer, BUFFSIZE, 0)))
	{
		send(sock_write, buffer, l, 0);
	}
	free(buffer);
}

void session_start(int sockd)
{
	int port;
	int l;
	char* buffer=(char*) malloc (BUFFSIZE);
	session_get_type(sockd, &port, buffer, BUFFSIZE, &l);
	int sockc = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in clientaddr;
	clientaddr.sin_family = AF_INET;
	clientaddr.sin_addr.s_addr = inet_addr(host);
	clientaddr.sin_port = htons(port);
	if (connect(sockc,(struct sockaddr *) &clientaddr, sizeof(struct sockaddr_in)) < 0)
	{
		perror("\nerror connecting to target\n");
		close(sockd);
		return;
	}
	if (l > 0)
		send(sockc, buffer, l, 0);
	int cpid;
	if ((cpid = fork()) == 0) // child: read
	{
		session_forward(sockd, sockc);
	} else {
		session_forward(sockc, sockd);
	}
	shutdown(sockc, SHUT_RDWR);
	shutdown(sockd, SHUT_RDWR);
	wait(NULL);	// wait for child above
	close(sockc);	// close sockets
	close(sockd);
	if (cpid)
		printf("connection closed: %d\n", sockd);
	free(buffer);
	exit(EXIT_SUCCESS); // exit forked process
}
